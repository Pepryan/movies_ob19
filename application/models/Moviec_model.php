<?php

class Moviec_model extends CI_Model
{
    var $table = 'movie';
    function show_mc()
    {
        $hasil = $this->db->query("SELECT movie.mov_id, actor.act_id, actor.act_fname, actor.act_lname, movie_cast.role, movie.mov_title
            FROM actor
            JOIN movie
            JOIN movie_cast
            WHERE actor.act_id = movie_cast.act_id 
            AND movie.mov_id = movie_cast.mov_id
            ORDER BY movie_cast.act_id");
        return $hasil;
    }
    public function get_by_id($id)
    {
        $this->db->from($this->table);
        $this->db->where('mov_id', $id);
        $query = $this->db->get();

        return $query->row();
    }
}

<nav class="navbar navbar-static-top">
	<!-- Sidebar toggle button-->
	<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
		<span class="sr-only">Toggle navigation</span>
	</a>

	<div class="navbar-custom-menu">
		<ul class="nav navbar-nav">
			<li><a href="<?php echo base_url() ?>auth/logout" class="btn"><i class="fa fa-sign-out" aria-hidden="true"></i> Keluar</a></li>

		</ul>
	</div>
</nav>
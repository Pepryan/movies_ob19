<h3>Movie Cast Data</h3>
<br />
<i class='halflings-icon white zoom-in'></i>
<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Actor ID</th>
            <th>Movie ID</th>
            <th>Actor First Name</th>
            <th>Actor Last Name</th>
            <th>Actor Role</th>
            <th>Movie Title</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($data->result_array() as $i) :
            $id = $i['act_id'];
            $id2 = $i['mov_id'];
            $fname = $i['act_fname'];
            $lname = $i['act_lname'];
            $role = $i['role'];
            $title = $i['mov_title'];

        ?>
            <tr>
                <td><?php echo $id; ?> </td>
                <td><?php echo $id2; ?> </td>
                <td><?php echo $fname; ?> </td>
                <td><?php echo $lname; ?> </td>
                <td><?php echo $role; ?> </td>
                <td><?php echo $title; ?> </td>
                <td><?php echo '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Show" onclick="detail(' . "'" . $id2 . "'" . ')"><i class="glyphicon glyphicon-info-sign"></i> Detail Movie</a>'; ?></td>

            </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <th>Actor ID</th>
            <th>Movie ID</th>
            <th>Actor First Name</th>
            <th>Actor Last Name</th>
            <th>Actor Role</th>
            <th>Movie Title</th>
            <th>Action</th>
        </tr>
    </tfoot>
</table>

<script>
    $(document).ready(function() {
        $('#table').DataTable({
            "scrollX": true,
        });
    });

    function detail(mov_id) {
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url: "<?php echo site_url('member/moviec/show_detail/') ?>" + mov_id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {

                $('[name="mov_id"]').val(data.mov_id);
                $('[name="mov_title"]').val(data.mov_title);
                $('[name="mov_year"]').val(data.mov_year);
                $('[name="mov_time"]').val(data.mov_time);
                $('[name="mov_lang"]').val(data.mov_lang);
                $('[name="mov_dt_rel"]').datepicker('update', data.mov_dt_rel);
                $('[name="mov_rel_country"]').val(data.mov_rel_country);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Detail Movie'); // Set title to Bootstrap modal title

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error get data from ajax');
            }
        });
    }
</script>
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Actor Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="mov_id" />
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Movie Title</label>
                            <div class="col-md-9">
                                <input name="mov_title" disabled class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Movie Year</label>
                            <div class="col-md-9">
                                <input name="mov_year" disabled class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Movie Time</label>
                            <div class="col-md-9">
                                <input name="mov_time" disabled class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Movie Language</label>
                            <div class="col-md-9">
                                <textarea name="mov_lang" disabled class="form-control"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Movie Date Release</label>
                            <div class="col-md-9">
                                <input name="mov_dt_rel" disabled class="form-control datepicker" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Movie Release Country</label>
                            <div class="col-md-9">
                                <textarea name="mov_rel_country" disabled class="form-control"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Tutup</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
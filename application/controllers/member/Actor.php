<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Actor extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->check_login();
        $this->load->model('Actor_model', 'Actor');
        $this->load->library('session');
    }

    public function index()
    {
        $this->load->helper('url');
        $data = konfigurasi('Actor');
        $this->template->load('layouts/template', 'member/actor_view', $data);
    }


    public function ajax_list()
    {
        $list = $this->Actor->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $actor) {
            $no++;
            $row = array();
            // $row[] = $no;
            $row[] = $actor->act_fname;
            $row[] = $actor->act_lname;
            $row[] = $actor->act_gender;

            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_actor(' . "'" . $actor->act_id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_actor(' . "'" . $actor->act_id . "'" . ')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Actor->count_all(),
            "recordsFiltered" => $this->Actor->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($id)
    {
        $data = $this->Actor->get_by_id($id);
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $this->_validate();
        $data = array(
            'act_fname' => $this->input->post('act_fname'),
            'act_lname' => $this->input->post('act_lname'),
            'act_gender' => $this->input->post('act_gender'),
        );
        $insert = $this->Actor->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $this->_validate();
        $data = array(
            'act_fname' => $this->input->post('act_fname'),
            'act_lname' => $this->input->post('act_lname'),
            'act_gender' => $this->input->post('act_gender'),
        );
        $this->Actor->update(array('act_id' => $this->input->post('act_id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id)
    {
        $this->Actor->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }


    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('act_fname') == '') {
            $data['inputerror'][] = 'act_fname';
            $data['error_string'][] = 'Actor First Name is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('act_lname') == '') {
            $data['inputerror'][] = 'act_lname';
            $data['error_string'][] = 'Actor Last Name is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('act_gender') == '') {
            $data['inputerror'][] = 'act_gender';
            $data['error_string'][] = 'Please Select Actor Gender';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}

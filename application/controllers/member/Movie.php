<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Movie extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->check_login();
        $this->load->model('Movie_model', 'Movie');
        $this->load->helper('url');

        $this->load->library('session');
    }

    public function index()
    {
        $this->load->helper('url');
        $data = konfigurasi('Movie');
        $this->template->load('layouts/template', 'member/movie_view', $data);
    }

    public function ajax_list()
    {
        $list = $this->Movie->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $movie) {
            $no++;
            $row = array();

            $row[] = $movie->mov_title;
            $row[] = $movie->mov_year;
            $row[] = $movie->mov_time;
            $row[] = $movie->mov_lang;
            $row[] = $movie->mov_dt_rel;
            $row[] = $movie->mov_rel_country;

            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_movie(' . "'" . $movie->mov_id . "'" . ')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_movie(' . "'" . $movie->mov_id . "'" . ')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Movie->count_all(),
            "recordsFiltered" => $this->Movie->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_edit($id)
    {
        $data = $this->Movie->get_by_id($id);
        $data->mov_dt_rel = ($data->mov_dt_rel == '0000-00-00') ? '' : $data->mov_dt_rel; // if 0000-00-00 set tu empty for datepicker compatibility
        echo json_encode($data);
    }

    public function ajax_add()
    {
        $this->_validate();
        $data = array(
            'mov_title' => $this->input->post('mov_title'),
            'mov_year' => $this->input->post('mov_year'),
            'mov_time' => $this->input->post('mov_time'),
            'mov_lang' => $this->input->post('mov_lang'),
            'mov_dt_rel' => $this->input->post('mov_dt_rel'),
            'mov_rel_country' => $this->input->post('mov_rel_country'),
        );
        $insert = $this->Movie->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_update()
    {
        $this->_validate();
        $data = array(
            'mov_title' => $this->input->post('mov_title'),
            'mov_year' => $this->input->post('mov_year'),
            'mov_time' => $this->input->post('mov_time'),
            'mov_lang' => $this->input->post('mov_lang'),
            'mov_dt_rel' => $this->input->post('mov_dt_rel'),
            'mov_rel_country' => $this->input->post('mov_rel_country'),
        );
        $this->Movie->update(array('mov_id' => $this->input->post('mov_id')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_delete($id)
    {
        $this->Movie->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }


    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('mov_title') == '') {
            $data['inputerror'][] = 'mov_title';
            $data['error_string'][] = 'Movie Title is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('mov_year') == '') {
            $data['inputerror'][] = 'mov_year';
            $data['error_string'][] = 'Movie Year is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('mov_time') == '') {
            $data['inputerror'][] = 'mov_time';
            $data['error_string'][] = 'Movie Time is required';
            $data['status'] = FALSE;
        }
        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}

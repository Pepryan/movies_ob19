<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Moviec extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->check_login();
        $this->load->model('Moviec_model', 'Moviec');
        $this->load->library('session');
    }

    public function index()
    {
        $this->load->helper('url');
        $data = konfigurasi('Movie Cast');
        $data['data'] = $this->Moviec->show_mc();
        $this->template->load('layouts/template', 'member/moviec_view', $data);
    }
    public function show_detail($id)
    {
        $this->load->model('Moviec_model', 'Moviec');
        $data = $this->Moviec->get_by_id($id);
        echo json_encode($data);
    }
}
